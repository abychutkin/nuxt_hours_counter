export const state = () => ({
    days: []
});

export const mutations = {
    pushDay(state, day) {
        state.days.push(day);
    },
    setStartHour(state, data) {
        let {index, value} = data;
        state.days[index]['start'] = value;
    },
    setEndHour(state, data) {
        let {index, value} = data;
        state.days[index]['end'] = value;
    }
}